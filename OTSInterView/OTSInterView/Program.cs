﻿using OTSInterView.Database;
using OTSInterView.Insfrastracture;
using OTSInterView.Models;
using OTSInterView.Services;

namespace OTSInterView;

internal sealed class Program
{
    static void Main(string[] args)
    {
        //Αρχικοποίηση του DataStore
        IDataStore<JobPosition> jobPositionDataStore = new JobPositionDataStore();
        IDataStore<Candidate> candidateDataStore = new CandidateDataStore();
        IDataStore<JobApply> jobApplyDataStore = new JobAppliesDataStore();

        //Δημιουργία των services
        var jobPositionsService = new JobPositionsService(jobPositionDataStore);

        //Δημιουργεια θεσης εργασιας
        var jobPosition = new JobPosition
        {
            Title = "Software Engineer",
            Address = "Αθήνα",
            Category = "Πληροφορική",
            City = "Αθήνα",
            CompanyDetails = "Εταιρία Πληροφορικής",
            IsAvailableForBuisnessTrips = true,
            IsDrivingLicenceRequired = true,
            IsEnabled = true,
            MinimumRequiredYearsOfExperience = 2,
            MinimumRequirements = 2,
            UploadDate = DateTime.Now,
            Description = "Περιγραφή θέσης εργασίας",
        };

        jobPositionsService.CreateJobPosition(jobPosition);



        var all = jobPositionsService.GetJobPositions();
        //Απενεργποιηση θεσης εργασιας
        jobPositionsService.DisableJobPosition(all.First().Id);


        //Δημιοργεια υποψηφιου

        var candidateService = new CandidateService(candidateDataStore, jobApplyDataStore, jobPositionDataStore);
        var candidate = new Candidate
        {
            City = "Αθήνα",
            ExperienceYears = 3,
            HasDrivingLicence = true,
            Address = "Αθήνα",
            Email = "Test@test.com",
            FullName = "Παναγιώτης Παπαδόπουλος",
            PhoneNumber = "1234567890",
            IsAvailableForBuisnessTrips = true,
            Job = "Πληροφορική",
        };

        candidateService.CreateCandidate(candidate);

        var allCandidates = candidateService.GetCandidates();

        //Υποβολή αίτησης για θέση εργασίας
        var applyResult = candidateService.ApplyForJob(allCandidates.First().Id, all.First().Id);

        if (applyResult)
        {
            Console.WriteLine("Η αίτηση υποβλήθηκε επιτυχώς");
        }
        else
        {
            Console.WriteLine("Η αίτηση δεν υποβλήθηκε επιτυχώς");
        }

        Console.WriteLine("Δοκιμη ιδιας αιτησης");
        Console.WriteLine("==================");


        var anotherTry = candidateService.ApplyForJob(allCandidates.First().Id, all.First().Id);
        if (anotherTry)
        {
            Console.WriteLine("Η αίτηση υποβλήθηκε επιτυχώς");
        }
        else
        {
            Console.WriteLine($"Εχετε είδη υποβαλει");
        }

        //Προτάσεις για υποψήφιο
        var enabledJobPositions = jobPositionsService.GetAvailableJobPositions();
        var suggestions = EvaluationProcess.GetSuggestions(allCandidates.First(), enabledJobPositions);
        Console.WriteLine("============================");
        Console.WriteLine("Προτάσεις για υποψήφιο");
        Console.WriteLine(candidate.FullName);
        foreach (var suggestion in suggestions)
        {
            Console.WriteLine("============================");
            Console.WriteLine("Προτεινόμενη θέση εργασίας");
            Console.WriteLine(suggestion.Job.Title);
            Console.WriteLine(suggestion.Job.Description);
            Console.WriteLine("============================");
        }

        Console.WriteLine("Πατήστε κάποιο πλήκτρο για να τερματίσετε");


        Console.ReadLine();
    }
}
