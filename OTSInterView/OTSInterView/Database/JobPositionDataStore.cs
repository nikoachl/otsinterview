﻿using OTSInterView.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTSInterView.Database
{
    /// <summary>
    /// Dumy DataStore για την αποθήκευση των θέσεων εργασίας
    /// </summary>
    public sealed class JobPositionDataStore : IDataStore<JobPosition>
    {
        private List<JobPosition> _jobPositions = new List<JobPosition>();


        public JobPositionDataStore()
        {
            Initialize();
        }


        /// <summary>
        /// Προσθήκη νέας θέσης εργασίας
        /// </summary>
        /// <param name="item"></param>
        /// <exception cref="NotImplementedException"></exception>
        public void Add(JobPosition item)
        {
            // Έλεγχος για το αν το αντικείμενο είναι null
            if (item == null) return;

            //Προσθηκη αν ειναι καινουργιο αντικειμενο 
            if (string.IsNullOrEmpty(item.Id))
            {
                item.Id = Guid.NewGuid().ToString();
                _jobPositions.Add(item);
                return;
            }
            //Ενημέρωση αν ειναι υπαρχον
            Update(item);
        }

        /// <summary>
        /// Διαγραφή θέσης εργασίας
        /// </summary>
        /// <param name="id"></param>
       
        public void Delete(string id)
        {
                var item = _jobPositions.FirstOrDefault(x => x.Id ==id.ToString());

            if (item != null) _jobPositions.Remove(item);
                
            
        }

        /// <summary>
        /// Επιστροφή όλων των θέσεων εργασίας
        /// </summary>
        /// <returns></returns>
      
        public IEnumerable<JobPosition> GetAll()
        {
            //Eπιστροφή όλων των θέσεων εργασίας
           return _jobPositions.ToList();
        }

        /// <summary>
        /// Επιστροφή θέσης εργασίας με βάση το id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
      
        public JobPosition GetById(string id)
        {
           return _jobPositions.FirstOrDefault(x => x.Id == id.ToString());
        }

        /// <summary>
        /// Ενημέρωση υπάρχουσας θέσης εργασίας
        /// </summary>
        /// <param name="item"></param>
      
        public void Update(JobPosition item)
        {
           if(item == null) return;

           var exists = _jobPositions.Any(x => x.Id == item.Id);
            if(exists)
            {
                //Εύρεση του αντικειμένου και αντικατάσταση του
                var index = _jobPositions.FindIndex(x => x.Id == item.Id);
                _jobPositions[index] = item;
            }
          
        }

        //Dummy Data on Initialize
        public void Initialize()
        {
            _jobPositions.Add(new JobPosition
            {
                Id = Guid.NewGuid().ToString(),
                Title = "Προγραμματιστής",
                Description = "Ζητείται προγραμματιστής με εμπειρία σε C#",
                Address = "Αθήνα",
                Category = "Πληροφορική",
                City = "Αθήνα",
                CompanyDetails = "Εταιρία Πληροφορικής",
                IsAvailableForBuisnessTrips = true,
                IsDrivingLicenceRequired = true,
                MinimumRequiredYearsOfExperience = 2,
                MinimumRequirements = 2,
                UploadDate = DateTime.Now,
                IsEnabled = true
            });

            _jobPositions.Add(new JobPosition
            {
                Id = Guid.NewGuid().ToString(),
                Title = "Σχεδιαστής",
                Description = "Ζητείται σχεδιαστής με εμπειρία σε Photoshop",
                Address = "θεσσαλονικη",
                Category = "Πληροφορική",
                City = "Θεσσαλονικη",
                CompanyDetails = "Εταιρία Πληροφορικής",
                IsAvailableForBuisnessTrips = true,
                IsDrivingLicenceRequired = true,
                MinimumRequiredYearsOfExperience = 2,
                MinimumRequirements = 2,
                UploadDate = DateTime.Now,
                IsEnabled = true
            });

            _jobPositions.Add(new JobPosition
            {
                Id = Guid.NewGuid().ToString(),
                Title = "Πωλητής",
                Description = "Ζητείται πωλητής με εμπειρία σε πωλήσεις",
                Address = "Αθήνα",
                Category = "Πωλήσεις",
                City = "Αθήνα",
                CompanyDetails = "Εταιρία Πωλήσεων",
                IsAvailableForBuisnessTrips = true,
                IsDrivingLicenceRequired = true,
                MinimumRequiredYearsOfExperience = 2,
                MinimumRequirements = 2,
                UploadDate = DateTime.Now,
                IsEnabled = true
            });

        }
    }
}
