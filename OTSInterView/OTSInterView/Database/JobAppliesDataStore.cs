﻿using OTSInterView.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTSInterView.Database
{
    /// <summary>
    /// Dumy DataStore για την αποθήκευση των αιτήσεων εργασίας
    /// </summary>
    public sealed class JobAppliesDataStore : IDataStore<JobApply>
    {
        private List<JobApply> _jobApplies = new List<JobApply>();

        public void Add(JobApply item)
        {
            if (item == null) return;

            if (string.IsNullOrEmpty(item.Id))
            {
                item.Id = Guid.NewGuid().ToString();
                _jobApplies.Add(item);
                return;
            }
            Update(item);
        }

        public void Delete(string id)
        {
            var item = _jobApplies.FirstOrDefault(x => x.Id == id.ToString());

            if (item != null) _jobApplies.Remove(item);
        }

        public IEnumerable<JobApply> GetAll()
        {
            return _jobApplies.ToList();
        }

        public JobApply GetById(string id)
        {
            return _jobApplies.FirstOrDefault(x => x.Id == id.ToString());
        }

        public void Update(JobApply item)
        {
            if (item == null) return;

            var exists = _jobApplies.Any(x => x.Id == item.Id);
            if (exists)
            {
                var index = _jobApplies.FindIndex(x => x.Id == item.Id);
                _jobApplies[index] = item;
            }

        }

        
    }
}
