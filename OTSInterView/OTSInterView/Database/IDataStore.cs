﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTSInterView.Database
{
    /// <summary>
    /// Χρησιμοποιουμε interface για να μπορουμε να επεκτεινουμε την λειτουργικοτητα του DataStore μας,αν επιθημισουμε να αλαξουμε τον τροπο αποθηκευσης των δεδομενων με μια κανονικη βαση δεδομενων δεν θα χρειαστε να αλλαξουμε τον κωδικα μας απλα θα κανουμε inplement το interface μας στην κλαση που θα δημιουργησουμε για την βαση δεδομενων μας
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IDataStore<T> where T : class
    {
        /// <summary>
        /// Προσθήκη νέου αντικειμένου
        /// </summary>
        /// <param name="item"></param>
        void Add(T item);
        /// <summary>
        /// Ενημέρωση υπάρχοντος αντικειμένου
        /// </summary>
        /// <param name="item"></param>
        void Update(T item);
        /// <summary>
        /// Διαγραφή αντικειμένου
        /// </summary>
        /// <param name="id"></param>
        void Delete(string id);
        /// <summary>
        /// Επιστροφή αντικειμένου με βάση το id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T GetById(string id);
        /// <summary>
        /// Επιστροφή όλων των αντικειμένων
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> GetAll();

    }
}
