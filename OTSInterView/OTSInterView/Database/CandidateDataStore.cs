﻿using OTSInterView.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTSInterView.Database
{
    /// <summary>
    /// Dumy DataStore για την αποθήκευση των υποψηφίων
    /// </summary>
    public sealed class CandidateDataStore : IDataStore<Candidate>
    {
        private List<Candidate> _candidates = new List<Candidate>();


        public void Add(Candidate item)
        {
            if (item == null) return;

            if (string.IsNullOrEmpty(item.Id))
            {
                item.Id = Guid.NewGuid().ToString();
                _candidates.Add(item);
                return;
            }
            Update(item);
        }

        public void Delete(string id)
        {
            var item = _candidates.FirstOrDefault(x => x.Id == id.ToString());

            if (item != null)
                _candidates.Remove(item);
        }

        
        public IEnumerable<Candidate> GetAll()
        {
            return _candidates.ToList();
        }

        public Candidate GetById(string id)
        {
            return _candidates.FirstOrDefault(x => x.Id == id.ToString());
        }

        public void Update(Candidate item)
        {
            if (item == null) return;

            var exists = _candidates.Any(x => x.Id == item.Id);
            if (exists)
            {
                //Εύρεση του αντικειμένου και αντικατάσταση του
                var index = _candidates.FindIndex(x => x.Id == item.Id);
                _candidates[index] = item;
            }

        }
    }
}
