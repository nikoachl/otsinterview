﻿using OTSInterView.Models;

namespace OTSInterView.Insfrastracture;

public static class EvaluationProcess
{
    public static List<Suggestion> GetSuggestions(Candidate candidate , List<JobPosition> jobPositions)
    {
        
        var enabled = jobPositions.Where(x => x.IsEnabled == true).ToList();
        var meetsCategoryJobs = new List<JobPosition>();

        foreach (var jobPosition in enabled)
        {
            if(candidate.Job == jobPosition.Category)
            {
                meetsCategoryJobs.Add(jobPosition);
            }
        }
        var suggestions = new List<Suggestion>();

        foreach(var jobPosition in meetsCategoryJobs)
        {
            var temporarySocre = 0;
           

            if(candidate.City == jobPosition.City)
            {
                temporarySocre += 25;
            }

            if(candidate.ExperienceYears >= jobPosition.MinimumRequiredYearsOfExperience)
            {
                temporarySocre += 25;
            }

            if(candidate.HasDrivingLicence == jobPosition.IsDrivingLicenceRequired)
            {
                temporarySocre += 25;
            }

            if(candidate.IsAvailableForBuisnessTrips == jobPosition.IsAvailableForBuisnessTrips)
            {
                temporarySocre += 25;
            }
            
            var suggestion = new Suggestion
            {
                Job = jobPosition,
                Score = temporarySocre
            };

            suggestions.Add(suggestion);
                          
           }


        return suggestions.OrderByDescending(x=>x.Score).ToList();
    }     
       
}

   


