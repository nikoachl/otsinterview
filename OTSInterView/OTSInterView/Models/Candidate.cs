﻿namespace OTSInterView.Models
{
    public sealed class Candidate : BaseModel
    {
        /// <summary>
        /// Ονοματεπώνυμο του υποψήφιου
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// Διεύθυνση κατοικίας
        /// </summary>

        public string? Address { get; set; }

     /// <summary>
     /// Ηλεκτρονική διεύθυνση
     /// </summary>

        public string Email { get; set; }
        /// <summary>
        /// Επικοινωνιακός αριθμός
        /// </summary>
        public string? PhoneNumber { get; set; }


        #region Αποτελούν σημεία σύγκρισης
        /// <summary>
        /// Επαγγελματική θέση
        /// </summary>
        public string Job { get; set; }

        /// <summary>
        /// Πόλη κατοικίας
        /// </summary>

        public string City { get; set; }

        /// <summary>
        /// Χρόνια εμπειρίας
        /// </summary>
        public int ExperienceYears { get; set; }

        /// <summary>
        ///  Κατοχος διπλώματος οδήγησης
        /// </summary>

        public bool HasDrivingLicence { get; set; }

        /// <summary>
        /// Διαθεσιμότητα για ταξίδια εργασίας
        /// </summary>

        public bool IsAvailableForBuisnessTrips{ get; set; }

        
        #endregion



    }
}
