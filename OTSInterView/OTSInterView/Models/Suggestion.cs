﻿namespace OTSInterView.Models
{
    public class Suggestion
    {
        public JobPosition Job { get; set; }

        public int Score { get; set; }

    }
}
