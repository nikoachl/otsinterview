﻿namespace OTSInterView.Models
{
    public class BaseModel
    {
        /// <summary>
        /// Αναγνωριστικό του μοντέλου
        /// </summary>
        public string Id { get; set; }
    }
}
