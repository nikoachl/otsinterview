﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTSInterView.Models
{
    public class JobPosition : BaseModel
    {
        /// <summary>
        /// Τίτλος της θέσης εργασίας
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// ΠΕριγραφή της θέσης εργασίας
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// ελάχιστες απαιτήσεις
        /// </summary>
        public int MinimumRequirements { get; set; }

        /// <summary>
        /// Ημερομηνία ανάρτησης της θέσης εργασίας
        /// </summary>

        public DateTime UploadDate { get; set; }

        /// <summary>
        /// Στοιχέια εταιρίας που αναζητάει τον υπάλληλο
        /// </summary>
        public string CompanyDetails { get; set; }

        /// <summary>
        /// Διεύθυνση εργασίας
        /// </summary>

        public string? Address { get; set; }

        /// <summary>
        /// Δηνατότητα απενεργοποίησης της θέσης εργασίας
        /// </summary>
        public bool IsEnabled { get; set; }

        
        #region Αποτελούν σημεία σύγκρισης

        /// <summary>
        /// Πολη που βρίσκεται η εταιρία
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Κατηγορία της θέσης εργασίας
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// Ελαχιστα χρονια εμπειρίας που απαιτούνται
        /// </summary>

        public int MinimumRequiredYearsOfExperience { get; set; }

        /// <summary>
        /// Χρειάζεται δίπλωμα οδήγησης
        /// </summary>

        public bool IsDrivingLicenceRequired { get; set; }

        /// <summary>
        /// Ειναι διαθεσιμος για ε΅παγγελματικα ταξίδια
        /// </summary>
        public bool IsAvailableForBuisnessTrips { get; set; }

        #endregion
    }
}
