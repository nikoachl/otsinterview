﻿namespace OTSInterView.Models
{
    public sealed class JobApply : BaseModel
    {
        public string CandidateId { get; set; }

        public string JobPositionId { get; set; }

        public DateTime ApplyDate { get; set; }

    }
}
