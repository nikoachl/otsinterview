﻿using OTSInterView.Database;
using OTSInterView.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTSInterView.Services;

public sealed class JobPositionsService
{
    private IDataStore<JobPosition> _jobPositionsDataStore;

    /// <summary>
    /// Χρησιμοποιουμε dependency injection
    /// </summary>
    /// <param name="jobPositionsDataStore"></param>
    /// <exception cref="ArgumentNullException"></exception>
    public JobPositionsService(IDataStore<JobPosition> jobPositionsDataStore)
    {
        _jobPositionsDataStore = jobPositionsDataStore ?? throw new ArgumentNullException(nameof(jobPositionsDataStore));
    }

    #region Απαιτούμενες λειτουργίες
    //Δημιουργεια θεσης εργασιας
    public void CreateJobPosition(JobPosition jobPosition)
    {
        
        _jobPositionsDataStore.Add(jobPosition);
    }

    //Διαγραφη θεσης εργασιας
    public void DeleteJobPosition(string id)
    {
        _jobPositionsDataStore.Delete(id);
    }

    public List<JobPosition> GetJobPositions()
    {
        return _jobPositionsDataStore.GetAll().ToList();
    }

    //Διαθέσημες θεσης εργασιας
    public List<JobPosition> GetAvailableJobPositions()
    {
        return _jobPositionsDataStore.GetAll().Where(x => x.IsEnabled).ToList();
    }

    //Απενεργοποιηση θεσης εργασιας
    public void DisableJobPosition(string id)
    {
        var jobPosition = _jobPositionsDataStore.GetById(id);
        jobPosition.IsEnabled = false;
        _jobPositionsDataStore.Update(jobPosition);
    }

    #endregion

}

