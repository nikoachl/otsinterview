﻿using OTSInterView.Database;
using OTSInterView.Models;

namespace OTSInterView.Services;

public sealed class CandidateService
{
    private readonly IDataStore<Candidate> _candidateDataStore;
    private readonly IDataStore<JobPosition> _jobPositionsDataStore; 
    private readonly IDataStore<JobApply> _jobApplyDataStore;

    

    public CandidateService(IDataStore<Candidate> candidateDataStore, IDataStore<JobApply> jobApplyDataStore, IDataStore<JobPosition> jobPositionsDataStore)
    {
        _candidateDataStore = candidateDataStore ??
           throw new ArgumentNullException(nameof(candidateDataStore));
        _jobApplyDataStore = jobApplyDataStore ?? 
            throw new ArgumentException(nameof(jobApplyDataStore));
        _jobPositionsDataStore = jobPositionsDataStore ??
            throw new ArgumentNullException(nameof(jobPositionsDataStore));
    }
   

    #region Απαιτούμενες λειτουργίες
    public void CreateCandidate(Candidate candidate)
    {
        _candidateDataStore.Add(candidate);
    }

    public void DeleteCandidate(string id)
    {
        _candidateDataStore.Delete(id);
    }

    
    public List<Candidate> GetCandidates()
    {
        return _candidateDataStore.GetAll().ToList();
    }


    public bool ApplyForJob(string candidateId,string jobId)
    {
        List<JobApply> allApplies = _jobApplyDataStore.GetAll().ToList();
        

        //Ελεγχος αν η θεση εργασιας ειναι ενεργη
        var jobPosition = _jobPositionsDataStore.GetById(jobId);
        if (!jobPosition.IsEnabled)
            return false;

        //Ελεγχως αν εχει ειδη υποβαλει εταιση για την συγκεκριμενη θεση
        foreach (var apply in allApplies)
        {
            if (apply.CandidateId == candidateId && apply.JobPositionId == jobId)
                return false;
        }
       
      
        //Αλλη θεση συνεχιζει το proccess κανονικα
        var Apply = new JobApply
        {
            ApplyDate = DateTime.Now,
            CandidateId = candidateId,
            JobPositionId = jobId,
        };

        _jobApplyDataStore.Add(Apply);
        return true;
    }


    #endregion


}

